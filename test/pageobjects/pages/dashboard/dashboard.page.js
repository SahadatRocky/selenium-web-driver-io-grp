import Base from '../base';

class DashboardPage extends Base {
    
    get dashboardEventModule(){
       return $('img[src="assets/img/brand/event.png"]');
    }   
}

module.exports = new DashboardPage();
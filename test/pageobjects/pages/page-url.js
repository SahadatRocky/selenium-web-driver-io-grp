/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/
class PageUrl {
    /**
    * Opens a sub page of the page
    * @param path path of the sub page (e.g. /path/to/page.html)
    */
    // open (path) {
    //     return browser.url(`https://the-internet.herokuapp.com/${path}`)
    // }
    openLoginPageUrl(){
        browser.url('http://sqa.grp.gov.bd/global/web/login');

    }

    openDashboardPageUrl(){
       browser.url('http://sqa.grp.gov.bd/global/web/dashboard');
    }

    openCalendarPageUrl(){
        browser.url('http://sqa.grp.gov.bd/ictd/web/event/event/dashboards/calendar');
     }


    openEventListPageUrl(){
        browser.url('http://sqa.grp.gov.bd/ictd/web/event/event/dashboards/pre-event/pre-event-list');
        
     }

     

    
}

module.exports = new PageUrl();
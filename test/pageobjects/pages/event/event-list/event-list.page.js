import Base from '../../base';
/**
 * sub page containing specific selectors and methods for a specific page
 */
class EventlistPage extends Base {
    /**
     * define selectors using getter methods
     */

    // get createEventButton(){
    //     return $('button=নতুন ইভেন্ট');
    // }
    // get selectEventTitle(){
    //     return $('span=জাতীয় অনুষ্ঠান');
    // }
    // get clickEventType(){
    //     return $('p=মেলা');
    // }

    //concept-note
    get selectEventTypeDetailsOid(){
      return $('[formcontrolname="eventTypeDetailsOid"]');
    }
    get checkEventTypeDetailsOidItem(){
      return $('span=Business Automation Hackathon-2021');
    }
    get description(){
        return $('[formcontrolname="description"]');
    }
    get saveDraft(){
       return $('span=খস​ড়া সংরক্ষণ করুন');
    }
    get editBtn(){
        return $('body > app-dashboard > div > main > div > app-event-list > div > div > table > tbody > tr:nth-child(1) > td.mat-cell.cdk-column-actions.mat-column-actions.ng-star-inserted > a.btn.btn-secondary');
     }
     get nextBtn(){
        return $('span=পরবর্তী​');
     }

     //basic-info
     get referenceNo(){
        return $('[formcontrolname="referenceNo"]');
     }

     get external(){
        return $('[value="external"]');
     }

    get selectvenueOid(){
        return $('[formcontrolname="venueOid"]');
    }
    
    get clickvenueOidItem(){
        return $('span=ঢাকা আন্তর্জাতিক বাণিজ্য মেলার মাঠ');
    }
    get clickStartDate(){
        return $('[formcontrolname="startdate"]');
    }
    get selectStartDate(){
        return $('tr:nth-child(4) > td:nth-child(4) > div');
    }
    get clickEndDate(){
        return  $('[formcontrolname="enddate"]');
    }
    get selectEndDate(){
        return  $('tr:nth-child(4) > td:nth-child(4) > div');
    }
    get totalBudget(){
        return  $('[formcontrolname="totalBudget"]');
    }
    
    get saveBtn(){
        return  $('span=সংরক্ষণ করুন');
    }
    
    ////venue
    get addIconBtn(){
       return $('button > span > mat-icon');
    }
    get roomDesc(){
        return  $('[formcontrolname="roomDesc"]');
    }

    get venueCondition(){
        return $('[formcontrolname="venueCondition"]');
    }
    get advance(){
        return $('[formcontrolname="advance"]');
    } 
    get remarks(){
        return $('[formcontrolname="remarks"]');
    }   
    get clickStartTime(){
        return $('[formcontrolname="startTime"]');
    } 
    get clickStartkHours(){
        return $('span=11');
    }
    get clickStartAMorPM(){
        return $('button=AM');
    }
    get clickOKBtn(){
        return $('span=Ok');
    }
    get clickEndTime(){
        return $('[formcontrolname="endTime"]');
    }
    get clickEndkHours(){
        return $('span=3');
    }
    get clickEndAMorPM(){
        return $('button=PM');
    }

    get clickEmpId(){
        return $('[formcontrolname="empId"]');
    }
    
    get selectEmp(){
        return  $('span=Zamir Hossain,সিনিয়র প্রোগ্রামার/ডিবিএ,ইন্ডাস্ট্রি ইউনিট ,বিজনেস অটোমেশন টেস্ট অফিস');
    }

    get vanueSaveBtn(){
        return $('span=সংরক্ষণ করুন​');
    }
    
    //commitee
    get title(){
        return $('[formcontrolname="title"]');
    }
    get com_saveBtn(){
        return $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-committee-details-create/mat-card/mat-card-content/form/div[6]/button[2]/span');
    }

    //programschedule
    get scheduleEditBtn(){
        return $('tr:nth-child(1) > td.mat-cell > a.btn.btn-secondary');
    }
   get clickscheduleSession(){
       return $('[formcontrolname="scheduleSession"]');
   }
   get selectscheduleSession(){
    return $('span=দিনের প্রথম ভাগ'); 
   }
   get clickProgramStartTime(){
    return $('[formcontrolname="startTime"]');
  } 
  get clickProgramStartkHours(){
    return $('span=11');
  }
  get clickProgramStartAMorPM(){
    return $('button=AM');
  }
  get clickProgramOKBtn(){
    return $('span=Ok');
 }
 get clickProgramEndTime(){
    return $('[formcontrolname="endTime"]');
 }
get clickProgramEndkHours(){
    return $('span=3');
}
get clickProgramEndAMorPM(){
    return $('button=PM');
}

get purposes(){
    return  $('[formcontrolname="purposes"]');
}
get committeeOid(){
    return $('[formcontrolname="committeeOid"]');
}

get selectCommittee(){
    return $('span=Test1_comm_D12');  ///chnage???
}
get scheduleSavebtn(){
    return $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-program-schedule-details-create/mat-card/div/mat-card-actions/button[2]/span');
}

///checklist
get dueDateTime(){
    return $('[formcontrolname="dueDateTime"]');
}

get selectStartDate(){
    return $('tr:nth-child(4) > td:nth-child(4) > div');
}

get todoTitle(){
    return $('[formcontrolname="todoTitle"]');
}

get assignPersonOid(){
    return  $('[formcontrolname="assignPersonOid"]');
}
get selectAssignPerson(){
    return  $('span=Zamir Hossain,বিজনেস অটোমেশন টেস্ট অফিস');
}

get checklistSaveBtn(){
    return $('span=সংরক্ষন করুন');
}

///sponsor
get organizationDetails(){
    return  $('[name="organizationDetails"]');
}
get sponsorTypeName(){
    return  $('[name="sponsorTypeName"]');
}

get selectsponsorTypeName(){
    return $('span=ভেন্যু অংশীদার');
}

get sponsorCategory(){
    return $('[name="sponsorCategory"]');
}
get selecsponsorCategory(){
    return $('span=স্পনসর');
}

get sponsorShipName(){
    return $('[name="sponsorShipName"]');
}

get selecsponsorShipName(){
    return  $('span=টাইটানিয়াম');
}
get address(){
    return  $('[name="address"]');
}
get contactPerson(){
    return  $('[name="contactPerson"]');
}
get phone(){
    return  $('[name="phone"]');
}

get sponsorSaveBtn(){
    return  $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-sponsor-create/mat-card/mat-card-content/form/div[8]/div/div[2]/div/button[2]/span');
}


///members
get addComMember(){
    return $('app-members > mat-card > div > div > div.col-md-10 > button:nth-child(3)');
}

get clickfiltericon(){
    return $('mat-card-content:nth-child(1) > div > div.col-md-2 > mat-card-actions > button');
}
get inputfiltervalue(){
    return  $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-member-management/div/div[1]/mat-tab-group/div/mat-tab-body[1]/div/app-employees/mat-card[2]/mat-card-content[2]/div/div[1]/mat-form-field/div/div[1]/div/input');
}

get clicksearchicon(){
    return  $('mat-card-content:nth-child(2) > div > div.col-md-2 > mat-card-actions > a.btn.btn-primary.mx-1');
}

get clickdropdownforcommittee(){
    return  $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-member-management/div/div[1]/mat-tab-group/div/mat-tab-body[1]/div/app-employees/mat-card[1]/div/div[1]/mat-form-field/div/div[1]/div/mat-select');
}

get selectCommitee(){
    return  $('span=Test1_comm_D12'); ///chanage
}

get clickdropdownformember(){
    return  $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-member-management/div/div[1]/mat-tab-group/div/mat-tab-body[1]/div/app-employees/mat-card[1]/div/div[2]/mat-form-field/div/div[1]/div/mat-select');
}
get selectmember(){
    return $('span=সভাপতি');
}
get selectmember2(){
    return $('span=সদস্য সচিব');
}

get clickcheckbox(){
    return $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-member-management/div/div[1]/mat-tab-group/div/mat-tab-body[1]/div/app-employees/div/table/tbody/tr[1]/td[1]/mat-checkbox');
}
get clickcheckbox2(){
    return $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-member-management/div/div[1]/mat-tab-group/div/mat-tab-body[1]/div/app-employees/div/table/tbody/tr[2]/td[1]/mat-checkbox');
}

get membersSaveBtn(){
    return $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-member-management/div/div[2]/button[2]');
}



///importantperson
get addimportantPerson(){
    return $('/html/body/app-dashboard/div/main/div/app-pre-event-details/mat-tab-group/div/mat-tab-body[8]/div/app-members/mat-card/div/div/div[2]/button[2]');
}
get selectimportantmember(){
    return  $('span=চেয়ারপারসন');
}
get selectmemberPerson(){
    return  $('span=উদ্বোধনী অনুষ্ঠান');
}

get clickcheckboxPerson(){
    return  $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-member-management/div/div[1]/mat-tab-group/div/mat-tab-body[1]/div/app-employees/div/table/tbody/tr[3]/td[1]/mat-checkbox');
}

get selectimportantmember2(){
    return  $('span=প্রধান অতিথি');
}


get selectmemberPerson2(){
    return $('span=উদ্বোধনী অনুষ্ঠান');
}

get clickcheckboxPerson2(){
    return $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-member-management/div/div[1]/mat-tab-group/div/mat-tab-body[1]/div/app-employees/div/table/tbody/tr[6]/td[1]/mat-checkbox');
}


///budget

get addnewBudget(){
    return $('mat-icon=add');
}

get economicCode(){
    return $('[formcontrolname="economicCode"]');
}

get estimatedBudget(){
    return $('[formcontrolname="estimatedBudget"]');
}

get eventDate(){
    return  $('[formcontrolname="eventDate"]');
}
get selecteventDate(){
    return  $('tr:nth-child(4) > td:nth-child(4) > div');
}
get expenseTypeOid(){
    return  $('[formcontrolname="expenseTypeOid"]');
}
get selectexpenseType(){
    return  $('span=ইভেন্ট ভাড়া');
}

get budgetSaveBtn(){
    return $('span=সংরক্ষন করুন');
}


///ইভেন্ট অনুমোদনের জন্য প্রেরণ
get clickAction(){
    return $('app-speed-dial-fab > div > button');
}

get clickeventapprovalIcon(){
    return $('/html/body/app-dashboard/div/main/div/app-pre-event-details/app-speed-dial-fab/div[2]/div/button[8]');
}

get clickdropdownfromreviewer(){
    return $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-cancel-meeting/div/mat-card/mat-card-content/mat-form-field[1]/div/div[1]/div/mat-select');
}

get selectreviewer(){
    return  $('span=রিভিউয়ার-১ , Moudud Hasan , বিজনেস অটোমেশন টেস্ট অফিস');
}

get resonTextArea(){
    return  $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-cancel-meeting/div/mat-card/mat-card-content/mat-form-field[2]/div/div[1]/div/textarea');
}

get yesBtn(){
    return  $('/html/body/div[2]/div[2]/div/mat-dialog-container/app-cancel-meeting/div/mat-card/mat-card-actions/button[2]');
}

}

module.exports = new EventlistPage();
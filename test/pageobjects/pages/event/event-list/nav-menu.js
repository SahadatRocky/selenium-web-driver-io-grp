import Base from '../../base';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class NavMenu extends Base {
    /**
     * define selectors using getter methods
     */
    
     get eventList(){
         return $('a[href="/ictd/web/event/event/dashboards/pre-event/pre-event-list"]');
     }
     
     get todos(){
        return $('a[href="/ictd/web/event/event/todos/0"]');
     }

}

module.exports = new NavMenu();

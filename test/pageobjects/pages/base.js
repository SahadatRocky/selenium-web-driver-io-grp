

class Base {

    pauseShort(){
        browser.pause(3000);
    }
    pauseMedium(){
        browser.pause(5000);
    }

    pauseLong(){
        browser.pause(8000);
    }
    
    get snakbarMessage(){
        return $('simple-snack-bar>span');
    } 
}

export default Base;
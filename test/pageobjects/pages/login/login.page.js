import Base from '../base';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Base {
    /**
     * define selectors using getter methods
     */
    get inputUsername () { 
        return $('[name="username"]'); 
    }
    get inputPassword () { 
        return $('[name="password"]');
    }
    get btnSubmit () {
         return $('button=প্রবেশ করুন'); 
    }
    // get error(){
    //     return $('simple-snack-bar>span');
    // }    
    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    login (username, password) {
        this.inputUsername.waitForExist();
        this.inputUsername.setValue(username);
        this.inputPassword.waitForExist();
        this.inputPassword.setValue(password);
    }
}

module.exports = new LoginPage();

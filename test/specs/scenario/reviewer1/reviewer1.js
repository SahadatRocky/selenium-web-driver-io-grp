import PageUrl from '../../../pageobjects/pages/page-url';
import LoginPage from '../../../pageobjects/pages/login/login.page';
import DashboardPage from '../../../pageobjects/pages/dashboard/dashboard.page';
import NavMenu from '../../../pageobjects/pages/event/event-list/nav-menu';
import allureReporter from '@wdio/allure-reporter'
var objectUserInfo;
describe('test -reviewer -1',() => {
    it('should login with valid credentials', () => {

        allureReporter.addFeature('valid credentials');
        const observable = util.getLoginCSVData('test/specs/user_Data.csv','reviewer1');
        observable.subscribe({
            next: x =>  {
                console.log(x.username);
                objectUserInfo  = x;   
            },
            error: err => {
                console.log("Error : "+err);
            },
            complete: () => {
                console.log("Done...");
            }
        });
        PageUrl.openLoginPageUrl();
        expect(browser).toHaveUrl('http://sqa.grp.gov.bd/global/web/login');
        
        LoginPage.login(objectUserInfo.username,objectUserInfo.password);
        browser.waitAndClick(LoginPage.btnSubmit);
        LoginPage.pauseShort();
    });


    it('navigate Dashboard Url and click event module',() => {
        allureReporter.addFeature('navigate dashboard and click event module');
        PageUrl.openDashboardPageUrl();
        expect(browser).toHaveUrl('http://sqa.grp.gov.bd/global/web/dashboard');
        browser.waitAndClick(DashboardPage.dashboardEventModule);
        DashboardPage.pauseShort();
    });

    it('test todos-', () => {
        PageUrl.openCalendarPageUrl();
        expect(browser).toHaveUrl('http://sqa.grp.gov.bd/ictd/web/event/event/dashboards/calendar');
        browser.waitAndClick(NavMenu.todos);

        let click_anu = $('span=অনুমোদন');
        click_anu.waitForExist();
        click_anu.click();

        let clickview = $('/html/body/app-dashboard/div/main/div/app-todos/mat-tab-group/div/mat-tab-body[2]/div/app-event-approval/div/div/table/tbody/tr[1]/td[3]/a');
        clickview.waitForExist();
        clickview.click();

       let getText = $('div=Assaduzaman,বিজনেস অটোমেশন টেস্ট অফিস ।');
       expect(getText).toHaveUrl('Assaduzaman,বিজনেস অটোমেশন টেস্ট অফিস ।');
        NavMenu.pauseShort();
    });


});
import PageUrl from '../../../pageobjects/pages/page-url';
import LoginPage from '../../../pageobjects/pages/login/login.page';
import DashboardPage from '../../../pageobjects/pages/dashboard/dashboard.page';
import NavMenu from '../../../pageobjects/pages/event/event-list/nav-menu';
import EventlistPage from '../../../pageobjects/pages/event/event-list/event-list.page';
import allureReporter from '@wdio/allure-reporter'
import * as util from '../../../../lib/data-helpers';
const {Observable} = require('rxjs');
let objectUserInfo;
let eventTaskInfo; 
describe('should test for event create',() => {
   
    it('should not login with invalid credentials', () => {
    allureReporter.addFeature('invalid credentials');
    const observable = util.getLoginCSVData('test/specs/user_Data.csv','none');
    observable.subscribe({
        next: x =>  {
            console.log(x.username);
            objectUserInfo  = x;   
        },
        error: err => {
            console.log("Error : "+err);
        },
        complete: () => {
            console.log("Done...");
        }
        });

        PageUrl.openLoginPageUrl();
        expect(browser).toHaveUrl('http://sqa.grp.gov.bd/global/web/login');
        LoginPage.login(objectUserInfo.username,objectUserInfo.password);
        browser.waitAndClick(LoginPage.btnSubmit);
    
        expect(LoginPage.snakbarMessage).toHaveText('ব্যবহারকারীর আইডি অথবা পাসওয়ার্ড ভুল');
       LoginPage.pauseShort();
    });

   
    it('should login with valid credentials', () => {
        allureReporter.addFeature('valid credentials');
        let observable = util.getLoginCSVData('test/specs/user_Data.csv','creator');
        observable.subscribe({
            next: x =>  {
                console.log(x.username);
                objectUserInfo  = x;   
            },
            error: err => {
                console.log("Error : "+err);
            },
            complete: () => {
                console.log("Done...");
            }
        });
       
        PageUrl.openLoginPageUrl();
        expect(browser).toHaveUrl('http://sqa.grp.gov.bd/global/web/login');

        LoginPage.login(objectUserInfo.username,objectUserInfo.password);
        browser.waitAndClick(LoginPage.btnSubmit);
        LoginPage.pauseShort();
    });


    it('navigate Dashboard Url and click event module',() => {
        allureReporter.addFeature('navigate dashboard and click event module');
        PageUrl.openDashboardPageUrl();
        expect(browser).toHaveUrl('http://sqa.grp.gov.bd/global/web/dashboard');
        browser.waitAndClick(DashboardPage.dashboardEventModule);
        DashboardPage.pauseShort();
    });

    it('navigate calendar Url and click event-list', () => {
        allureReporter.addFeature('navigate calendar Url and click event-list');
        PageUrl.openCalendarPageUrl();
        expect(browser).toHaveUrl('http://sqa.grp.gov.bd/ictd/web/event/event/dashboards/calendar');
        browser.waitAndClick(NavMenu.eventList);
        NavMenu.pauseShort();
    });

    //working code --crate event
 
    it('navigate Eventlist Url and click new Event button', () => {
        allureReporter.addFeature('navigate Eventlist Url and click new Event button');
        let observable = util.getEventTaskCSVData('test/specs/scenario/event-create-task/event_list_task_data.csv','task1');
        observable.subscribe({
            next: x =>  {
                console.log('**',x);
                eventTaskInfo  = x;   
            },
            error: err => {
                console.log("Error : "+err);
            },
            complete: () => {
                console.log("Done...");
            }
        });
        
        PageUrl.openEventListPageUrl();
        expect(browser).toHaveUrl('http://sqa.grp.gov.bd/ictd/web/event/event/dashboards/pre-event/pre-event-list');
       
        let EventButton = $('button='+eventTaskInfo.eventButton);
        expect(EventButton).toHaveText(eventTaskInfo.eventButton);
        browser.waitAndClick(EventButton);
        EventlistPage.pauseShort();
    });
   
    it('test-select Event Name from cards', () => {
      
       allureReporter.addFeature('test- select event name-from cards');
       let selectEventTitle = $('span='+eventTaskInfo.eventTitle);
       expect(selectEventTitle).toHaveText(eventTaskInfo.eventTitle);
       let clickEventType = $('p='+eventTaskInfo.eventType);
       expect(clickEventType).toHaveText(eventTaskInfo.eventType);
       browser.waitAndClick(clickEventType);
       EventlistPage.pauseShort();
    });
    

    it('test-concept note', () => {
        // allureReporter.addFeature('test-concept note');
        // browser.waitAndClick(EventlistPage.selectEventTypeDetailsOid);
        // expect(EventlistPage.checkEventTypeDetailsOidItem).toHaveText(eventListTask[taskIndex]['EventTypeDetailsOidItem']);
        // browser.waitAndClick(EventlistPage.checkEventTypeDetailsOidItem);
        
        // browser.waitAndTypeText(EventlistPage.description,eventListTask[taskIndex]['description']);
        
        // browser.waitAndClick(EventlistPage.saveDraft);
        // browser.waitAndClick(EventlistPage.nextBtn);
        // NavMenu.pauseShort();
    });
    
    //----------------------------------------------------------  
    // it('test -event edit-button-', () => {
    // allureReporter.addFeature('test- event edit-button');
    // PageUrl.openEventListPageUrl();
    // expect(browser).toHaveUrl('http://sqa.grp.gov.bd/ictd/web/event/event/dashboards/pre-event/pre-event-list');
    // browser.waitAndClick(EventlistPage.editBtn);
    // browser.waitAndClick(EventlistPage.nextBtn);
    // });

/*

it('testing  basic Info', () => {
    allureReporter.addFeature('test-basic Info');
    EventlistPage.pauseShort();
    browser.waitAndTypeText(EventlistPage.referenceNo,eventListTask[taskIndex]['referenceNo']); ///change ???
    browser.waitAndClick(EventlistPage.external);
    browser.waitAndClick(EventlistPage.selectvenueOid);
    expect(EventlistPage.clickvenueOidItem).toHaveText('ঢাকা আন্তর্জাতিক বাণিজ্য মেলার মাঠ');
    browser.waitAndClick(EventlistPage.clickvenueOidItem);
    
    browser.waitAndClick(EventlistPage.clickStartDate);

    expect(EventlistPage.selectStartDate).toHaveText('১৭');
    browser.waitAndClick(EventlistPage.selectStartDate);
    browser.waitAndClick(EventlistPage.clickEndDate);

    expect(EventlistPage.selectEndDate).toHaveText('১৭');
    browser.waitAndClick(EventlistPage.selectEndDate);

    browser.waitAndTypeText(EventlistPage.totalBudget,'300000');
    browser.waitAndClick(EventlistPage.saveBtn);

    expect(EventlistPage.snakbarMessage).toHaveText('সফলভাবে সংরক্ষন করা হয়েছে');
    browser.waitAndClick(EventlistPage.nextBtn);
    EventlistPage.pauseShort();
});


it('test-venue',() => {
    allureReporter.addFeature('test-venue');
    browser.waitAndClick(EventlistPage.addIconBtn);
    EventlistPage.pauseLong();
    browser.waitAndTypeText(EventlistPage.roomDesc,'very -goood');
    browser.waitAndTypeText(EventlistPage.venueCondition,'very -goood');
    browser.waitAndTypeText(EventlistPage.advance,'50000');
    browser.waitAndTypeText(EventlistPage.advance,'50000');
    browser.waitAndTypeText(EventlistPage.remarks,'veneue veneue venue veneue venueeee-');

    browser.waitAndClick(EventlistPage.clickStartTime);
    browser.waitAndClick(EventlistPage.clickStartkHours);
    browser.waitAndClick(EventlistPage.clickStartAMorPM);
    browser.waitAndClick(EventlistPage.clickOKBtn);
    browser.waitAndClick(EventlistPage.clickEndTime);
    browser.waitAndClick(EventlistPage.clickEndkHours);
    browser.waitAndClick(EventlistPage.clickEndAMorPM);
    browser.waitAndClick(EventlistPage.clickOKBtn);

    browser.waitAndClick(EventlistPage.clickEmpId);
    expect(EventlistPage.selectEmp).toHaveText('Zamir Hossain,সিনিয়র প্রোগ্রামার/ডিবিএ,ইন্ডাস্ট্রি ইউনিট ,বিজনেস অটোমেশন টেস্ট অফিস');
    browser.waitAndClick(EventlistPage.selectEmp);
    //let save_btn = $('span=সংরক্ষণ করুন​');
    browser.waitAndClick(EventlistPage.vanueSaveBtn);
    expect(EventlistPage.snakbarMessage).toHaveText('সফলভাবে সংরক্ষন করা হয়েছে');
    
   });

   

  it('test -commitee', () => {
    allureReporter.addFeature('test -commitee');
    browser.waitAndClick(EventlistPage.nextBtn);
    browser.waitAndClick(EventlistPage.addIconBtn);
    browser.waitAndTypeText(EventlistPage.title,'Test1_comm_D12'); /// new entry plz chnage value reson conflict
    browser.waitAndTypeText(EventlistPage.description,'Test1_D12'); /// /// new entry plz chnage value reson conflict
    browser.waitAndTypeText(EventlistPage.remarks,'Test_Test_Test_test');
    browser.waitAndClick(EventlistPage.com_saveBtn);

    expect(EventlistPage.snakbarMessage).toHaveText('সফলভাবে সংরক্ষন করা হয়েছে');
    EventlistPage.pauseShort();
  
  });


   it('test -program schedule',()=> {
    allureReporter.addFeature('test -program schedule');
    browser.waitAndClick(EventlistPage.nextBtn);
    browser.waitAndClick(EventlistPage.scheduleEditBtn);
    browser.waitAndClick(EventlistPage.clickscheduleSession);
    browser.waitAndClick(EventlistPage.selectscheduleSession);

    browser.waitAndClick(EventlistPage.clickProgramStartTime);
    browser.waitAndClick(EventlistPage.clickProgramStartkHours);
    browser.waitAndClick(EventlistPage.clickProgramStartAMorPM);
    browser.waitAndClick(EventlistPage.clickProgramOKBtn);
    browser.waitAndClick(EventlistPage.clickProgramEndTime);
    browser.waitAndClick(EventlistPage.clickProgramEndkHours);
    browser.waitAndClick(EventlistPage.clickProgramEndAMorPM);
    browser.waitAndClick(EventlistPage.clickProgramOKBtn);

    browser.waitAndTypeText(EventlistPage.description,'Test_1');
    browser.waitAndTypeText(EventlistPage.purposes,'Test_1');
    browser.waitAndClick(EventlistPage.committeeOid);
    browser.waitAndClick(EventlistPage.selectCommittee);
    browser.waitAndClick(EventlistPage.scheduleSavebtn);
    expect(EventlistPage.snakbarMessage).toHaveText('সফলভাবে তৈরি হয়েছে');
    EventlistPage.pauseShort();

   });

   it('test -checklist',()=> {
    allureReporter.addFeature('test -checklist');
    browser.waitAndClick(EventlistPage.nextBtn);
    // EventlistPage.pauseShort();
    browser.waitAndClick(EventlistPage.addIconBtn);
    EventlistPage.pauseMedium();
    
    browser.waitAndTypeText(EventlistPage.title,'checklist-test');
    browser.waitAndClick(EventlistPage.dueDateTime);
    expect(EventlistPage.selectStartDate).toHaveText('১৭');
    browser.waitAndClick(EventlistPage.selectStartDate);

    browser.waitAndTypeText(EventlistPage.todoTitle,'checklist-ToDo-test');
    browser.waitAndClick(EventlistPage.assignPersonOid);
    expect(EventlistPage.selectAssignPerson).toHaveText('Zamir Hossain,বিজনেস অটোমেশন টেস্ট অফিস');
    browser.waitAndClick(EventlistPage.selectAssignPerson);
    browser.waitAndClick(EventlistPage.checklistSaveBtn);
    expect(EventlistPage.snakbarMessage).toHaveText('সফলভাবে তৈরী হয়েছে');
    EventlistPage.pauseShort();
 
   });

   it('test-sponsor',() => {
    allureReporter.addFeature('test-sponsor');
    browser.waitAndClick(EventlistPage.nextBtn);
    browser.waitAndClick(EventlistPage.addIconBtn);

    browser.waitAndTypeText(EventlistPage.organizationDetails,'DBBL');
    browser.waitAndClick(EventlistPage.sponsorTypeName);
    expect(EventlistPage.selectsponsorTypeName).toHaveText('ভেন্যু অংশীদার');
    browser.waitAndClick(EventlistPage.selectsponsorTypeName);
    browser.waitAndClick(EventlistPage.sponsorCategory);

    expect(EventlistPage.selecsponsorCategory).toHaveText('স্পনসর');
    browser.waitAndClick(EventlistPage.selecsponsorCategory);
    browser.waitAndClick(EventlistPage.sponsorShipName);
    expect(EventlistPage.selecsponsorShipName).toHaveText('টাইটানিয়াম');
    browser.waitAndClick(EventlistPage.selecsponsorShipName);
    browser.waitAndTypeText(EventlistPage.address,'test-address');
    browser.waitAndTypeText(EventlistPage.contactPerson,'test-contactPerson');
    browser.waitAndTypeText(EventlistPage.phone,'01729942905');
    browser.waitAndTypeText(EventlistPage.remarks,'remarks');
    browser.waitAndClick(EventlistPage.sponsorSaveBtn);
    
    expect(EventlistPage.snakbarMessage).toHaveText('সফলভাবে তৈরী হয়েছে');
    EventlistPage.pauseShort();
 
   });



   it('test-members',() => {
    allureReporter.addFeature('test-members');
    browser.waitAndClick(EventlistPage.nextBtn);
    EventlistPage.pauseShort();

    ////////////////////////
    browser.waitAndClick(EventlistPage.addComMember);
    browser.waitAndClick(EventlistPage.clickfiltericon);
    browser.waitAndTypeText(EventlistPage.inputfiltervalue,'business');
    browser.waitAndClick(EventlistPage.clicksearchicon);
    browser.waitAndClick(EventlistPage.clickdropdownforcommittee);
    browser.waitAndClick(EventlistPage.selectCommitee);
    browser.waitAndClick(EventlistPage.clickdropdownformember);
    browser.waitAndClick(EventlistPage.selectmember);
    browser.waitAndClick(EventlistPage.clickcheckbox);
    browser.waitAndClick(EventlistPage.membersSaveBtn);
    
    ////////////////////////////////////////
  
    browser.waitAndClick(EventlistPage.addComMember);
    browser.waitAndClick(EventlistPage.clickfiltericon);
    browser.waitAndTypeText(EventlistPage.inputfiltervalue,'business');
    browser.waitAndClick(EventlistPage.clicksearchicon);
    browser.waitAndClick(EventlistPage.clickdropdownforcommittee);
    browser.waitAndClick(EventlistPage.selectCommitee);
    browser.waitAndClick(EventlistPage.clickdropdownformember);
    browser.waitAndClick(EventlistPage.selectmember2);
    browser.waitAndClick(EventlistPage.clickcheckbox2);
    browser.waitAndClick(EventlistPage.membersSaveBtn);

    ///////////////////////////////// 
    browser.waitAndClick(EventlistPage.addimportantPerson);
    browser.waitAndClick(EventlistPage.clickfiltericon);
    browser.waitAndTypeText(EventlistPage.inputfiltervalue,'business');
    browser.waitAndClick(EventlistPage.clicksearchicon);
    browser.waitAndClick(EventlistPage.clickdropdownforcommittee);
    browser.waitAndClick(EventlistPage.selectimportantmember);
    browser.waitAndClick(EventlistPage.clickdropdownformember);
    browser.waitAndClick(EventlistPage.selectmemberPerson);
    browser.waitAndClick(EventlistPage.clickcheckboxPerson);
    browser.waitAndClick(EventlistPage.membersSaveBtn);

    /////////////////////////
    browser.waitAndClick(EventlistPage.addimportantPerson);
    browser.waitAndClick(EventlistPage.clickfiltericon);
    browser.waitAndTypeText(EventlistPage.inputfiltervalue,'business');
    browser.waitAndClick(EventlistPage.clicksearchicon);
    browser.waitAndClick(EventlistPage.clickdropdownforcommittee);
    browser.waitAndClick(EventlistPage.selectimportantmember2);
    browser.waitAndClick(EventlistPage.clickdropdownformember);
    browser.waitAndClick(EventlistPage.selectmemberPerson2);
    browser.waitAndClick(EventlistPage.clickcheckboxPerson2);
    browser.waitAndClick(EventlistPage.membersSaveBtn);
    ///////////////////////
    EventlistPage.pauseShort();
   });
    
   it('testing budget',()=>{
    allureReporter.addFeature('testing budget');
    browser.waitAndClick(EventlistPage.nextBtn);
    browser.waitAndClick(EventlistPage.addnewBudget);
    browser.waitAndTypeText(EventlistPage.economicCode,'test-budget');
    browser.waitAndTypeText(EventlistPage.estimatedBudget,'150000');
    browser.waitAndClick(EventlistPage.eventDate);
    expect(EventlistPage.selecteventDate).toHaveText('১৭');
    browser.waitAndClick(EventlistPage.selecteventDate);
    browser.waitAndClick(EventlistPage.expenseTypeOid);
    browser.waitAndClick(EventlistPage.selectexpenseType);
    browser.waitAndTypeText(EventlistPage.remarks,'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    browser.waitAndClick(EventlistPage.budgetSaveBtn);

    EventlistPage.pauseShort();
   });

   it('ইভেন্ট অনুমোদনের জন্য প্রেরণ', () => {
    allureReporter.addFeature('ইভেন্ট অনুমোদনের জন্য প্রেরণ');
    browser.waitAndClick(EventlistPage.clickAction);
    browser.waitAndClick(EventlistPage.clickeventapprovalIcon);
    browser.waitAndClick(EventlistPage.clickdropdownfromreviewer);
    expect(EventlistPage.selectreviewer).toHaveText('রিভিউয়ার-১ , Moudud Hasan , বিজনেস অটোমেশন টেস্ট অফিস');
    browser.waitAndClick(EventlistPage.selectreviewer);
    browser.waitAndTypeText(EventlistPage.resonTextArea,'check for approval');
    browser.waitAndClick(EventlistPage.yesBtn);
    EventlistPage.pauseShort();
   });

   */
});    
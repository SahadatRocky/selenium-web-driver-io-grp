
module.exports = {
    browser: 'chrome',
    baseUrl: 'http://localhost',
    logLevel: 'info',
    maxInstance: 10,
    timeout: 10000,
    bail: 0
}
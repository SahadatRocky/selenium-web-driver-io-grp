const csv = require('csv-parser');
const fs = require('fs');
const {Observable} = require('rxjs');
module.exports = {
    getRandomName:function(){
        let randomNumber = Math.floor(Math.random()*100+1);
        return `name${new Date().getTime().toString()}${randomNumber}`;
    },

    getRandomId: function(){
        let randomNumber = Math.floor(Math.random()*1000 + 1);
        return `ID_${new Date().getTime().toString()}${randomNumber}`;
    },

    getRandomPhoneNumber: function(){
        return Math.random().toString().slice(2,11);
    },

    getRandomEmail: function(){
        return `${new Date().getTime().toString()}${'@email.com'}`;
    },

    getLoginCSVData: function(csvFileName,role){
        let results = [];
        return new Observable(subscriber =>{
            fs.createReadStream(csvFileName)
            .pipe(csv({}))
            .on('data', (data) => results.push(data))
            .on('end', () => {
            let res = results.filter(d=>d['role']==role);
            subscriber.next(res[0]);
            //console.log('****',res);  
           });
        });
    },
    
    getEventTaskCSVData: function(csvFileName,task){
        let results = [];
        return new Observable(subscriber =>{
            fs.createReadStream(csvFileName)
            .pipe(csv({}))
            .on('data', (data) => results.push(data))
            .on('end', () => {
            let res = results.filter(d=>d['task']==task);
            subscriber.next(res[0]);
            //console.log('****',res);  
           });
        });
    }

}